ReleaseSettings.globalReleaseSettings
ReleaseSettings.buildReleaseSettings(
  "JRubyConcurrentConstantMemoryExcel helps you to write Excel files as fast as possible without blowing your heap.",
  "MIT",
  "http://opensource.org/licenses/MIT",
  "JRubyConcurrentConstantMemoryExcel"
)

ThisBuild / developers := List(Developers.julesIvanic)

